extern crate chrono;
extern crate futures;
extern crate hyper;
extern crate hyper_tls;
extern crate rand;
extern crate tokio_core;

use std::thread;
use std::env;
use futures::Future;
use chrono::{DateTime, Utc};
use hyper_tls::HttpsConnector;
use hyper::header::{ContentLength, ContentType};
use hyper::{Client, Method, Request, StatusCode};
use std::time::Duration;
use std::sync::mpsc;
use tokio_core::reactor::Core;
use rand::distributions::{IndependentSample, Normal};

fn post_timestamp(
    url: &String,
    core: &mut Core,
    client: &hyper::Client<hyper_tls::HttpsConnector<hyper::client::HttpConnector>>,
    timestamp: DateTime<Utc>,
    pressure: f32
) {
    let json = format!("{{\"time\":\"{}\",\"press\":{}}}", timestamp.format("%FT%T%.9fZ"), pressure);

    let uri = url.parse().unwrap();
    let mut request = Request::new(Method::Post, uri);
    request.headers_mut().set(ContentType::json());
    request.headers_mut().set(ContentLength(json.len() as u64));
    request.set_body(json);

    core.run(client.request(request).map(|res| {
        if let StatusCode::Ok = res.status() {
            println!("INFO: POST done @{}", timestamp);
        } else {
            println!("ERR: POST failed @{}", timestamp);
        }
    })).unwrap_or_else(|err| println!("ERR: Couldn't post event: {}", err));
}

fn post_timestamps(rx: mpsc::Receiver<DateTime<Utc>>, url: String) {
    let mut core = Core::new().expect("ERR: Couldn't instantiate an event loop!");
    let handle = core.handle();
    let client = Client::configure()
        .connector(HttpsConnector::new(4, &handle).expect("ERR: Failed to initialize TLS"))
        .build(&handle);
    let mut rng = rand::thread_rng();
    let normal = Normal::new(90.0, 1.0);

    while let Ok(timestamp) = rx.recv() {
        let pressure = normal.ind_sample(&mut rng);
        println!("INFO: pressure: {} @{}", pressure, timestamp);
        post_timestamp(&url, &mut core, &client, timestamp, pressure as f32);
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let url = String::from(&args[1][..]);

    let mut rng = rand::thread_rng();
    let normal = Normal::new(0.5, 1.0);

    let (tx, rx) = mpsc::channel();
    thread::spawn(|| post_timestamps(rx, url));

    loop {
        let timestamp = Utc::now();
        println!("INFO: event @{}", timestamp);
        tx.send(timestamp).expect("ERR: Couldn't send timestamp from main thread");

        let duration = normal.ind_sample(&mut rng);
        if duration < 0.0 {
            continue;
        }

        let duration = (duration * 1000.0) as u64;
        thread::sleep(Duration::from_millis(duration))
    }
}
